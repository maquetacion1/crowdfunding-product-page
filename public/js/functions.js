(function( $ ) {
    $(document).ready(function (){
        let mainNavigation= document.getElementsByClassName("main-navigation");
        let menutoggle = document.getElementsByClassName("menu-toggle");
        let bookmark= document.getElementsByClassName("entry-bookmark");

        let checkboxes = document.querySelectorAll(".checkbox");
        let products = document.querySelectorAll(".entry-product");
        let modal = document.querySelector(".entry-modal");

        // Menu de navegacion
        $(document).on('click','.menu-toggle',function(event){
            event.preventDefault();
            // Cuando el menu se cierra
            if(mainNavigation[0].classList.contains("open")){
                $(".bg-inactive").css("display", "none");
                mainNavigation[0].classList.remove("open");
                $(".main-navigation").slideUp('slow');
                menutoggle[0].innerHTML =
                '<img src="./images/icon-hamburger.svg" alt="close menu button" />';
            } // Cuando abrimos el menu
            else {
                $(".bg-inactive").css("display", "block");
                $(".main-navigation").slideDown('slow');
                mainNavigation[0].className += " open";
                $(".main-navigation").css("display",'grid');
                menutoggle[0].innerHTML =
                '<img src="./images/icon-close-menu.svg" alt="close menu button" />';
            }
            
        });

        // Se muestra la modal principal
        $(document).on('click','.entry-modal-button',function(event){
            event.preventDefault();
            deleteClasses();
            disableCheckBoxes();
            $(".entry-modal").css("display",'flex');
            $(".bg-inactive").css("display", "block");
        });

        // Se cierra la modal principal
        $(document).on('click','.entry-button-close',function(event){
            event.preventDefault();
            $(".bg-inactive").css("display", "none");
            $(".entry-modal").css("display",'none');
        });

        // Se muestra la modal de fin
        $(document).on('click','.entry-reward-button',function(event){
            event.preventDefault();
            $(".entry-modal").css("display",'none');
            $(".entry-modal-success").css("display",'grid');
            $(".bg-inactive").css("display", "block");
        });

        // Se cierra la modal de fin
        $(document).on('click','.close-modal-success-button',function(event){
            event.preventDefault();
            $(".bg-inactive").css("display", "none");
            $(".entry-modal-success").css("display",'none');
        });

        checkboxes.forEach(checkbox => {
            checkbox.addEventListener('change', function() {
                // Cuando pulsamos un producto, unicamente se despliega su pedido y cerramos los demas si los hubiera
                if( $(this).is(':checked') ){
                    deleteClasses();
                    const idProduct = $(this).parent().parent()[0].id;
                    // Unicamente expandir los productos con recompensa
                    if(idProduct != 'no-reward'){
                        modal.className += ' modal-expanded';
                        $(this).parent().parent().addClass('product-selected');
                        const pledge = document.getElementsByClassName(idProduct);
                        pledge[0].className += " pledge-open";
                    } else {
                        $(this).parent().parent().addClass('product-selected-no-reward');
                    }
                }
            });
        })

        // Desmarcar todos los checkbox
        function disableCheckBoxes() {
            document.querySelectorAll('.entry-input input[type=radio]').forEach(function(checkElement) {
                checkElement.checked = false;
            });
        }

        // Cambio de color del input
        $(document).on('focus','input[type="text"]',function(event){
            $(this).parent().addClass('input-selected');
        })

        function deleteClasses(){
            products.forEach(product=>{
                product.classList.remove('product-selected');
                product.classList.remove('product-selected-no-reward');
            })
            const pledges_open = document.querySelectorAll(".pledge-open");
            pledges_open.forEach(pledge_open=>{
                pledge_open.classList.remove('pledge-open');
            })
            modal.classList.remove('modal-expanded');
            let inputText = document.querySelector(".input-selected");
            if(inputText) inputText.classList.remove('input-selected');
        }

        $(document).on('click','.entry-bookmark',function(event){
            if(bookmark[0].classList.contains("selected")){
                bookmark[0].classList.remove("selected");
                bookmark[0].lastElementChild.innerHTML="Bookmark";
            } else {
                bookmark[0].className += " selected";
                bookmark[0].lastElementChild.innerHTML="Bookmarked";
            }
        })
    })
})( jQuery );